__GIT_STATUS_CMD=`dirname $0`/gitstatus.sh

function replaceSymbols() {
  # Disable globbing, so a * could be used as symbol here
  set -f

  if [[ -z ${GIT_PROMPT_SYMBOLS_NO_REMOTE_TRACKING} ]]; then
    GIT_PROMPT_SYMBOLS_NO_REMOTE_TRACKING=L
  fi

  local VALUE=${1//_AHEAD_/${GIT_PROMPT_SYMBOLS_AHEAD}}
  local VALUE1=${VALUE//_BEHIND_/${GIT_PROMPT_SYMBOLS_BEHIND}}
  local VALUE2=${VALUE1//_NO_REMOTE_TRACKING_/${GIT_PROMPT_SYMBOLS_NO_REMOTE_TRACKING}}

  echo ${VALUE2//_PREHASH_/${GIT_PROMPT_SYMBOLS_PREHASH}}

  # reenable globbing symbols
  set +f
}

define_undefined_git_prompt_colors() {
  if [ -z ${GIT_PROMPT_SEPARATOR+x} ]; then GIT_PROMPT_SEPARATOR=" | "; fi              # separates each item

  if [ -z ${GIT_PROMPT_BRANCH+x} ]; then GIT_PROMPT_BRANCH=""; fi        # the git branch that is active in the current directory
  if [ -z ${GIT_PROMPT_STAGED+x} ]; then GIT_PROMPT_STAGED="●"; fi           # the number of staged files/directories
  if [ -z ${GIT_PROMPT_CONFLICTS+x} ]; then GIT_PROMPT_CONFLICTS="✖ "; fi       # the number of files in conflict
  if [ -z ${GIT_PROMPT_CHANGED+x} ]; then GIT_PROMPT_CHANGED="✚ "; fi        # the number of changed files

  if [ -z ${GIT_PROMPT_REMOTE+x} ]; then GIT_PROMPT_REMOTE=" "; fi                 # the remote branch name (if any) and the symbols for ahead and behind
  if [ -z ${GIT_PROMPT_UNTRACKED+x} ]; then GIT_PROMPT_UNTRACKED="…"; fi       # the number of untracked files/dirs
  if [ -z ${GIT_PROMPT_STASHED+x} ]; then GIT_PROMPT_STASHED="⚑ "; fi    # the number of stashed files/dir
  if [ -z ${GIT_PROMPT_CLEAN+x} ]; then GIT_PROMPT_CLEAN="✔"; fi      # a colored flag indicating a "clean" repo

  # Please do not add colors to these symbols
  if [ -z ${GIT_PROMPT_SYMBOLS_AHEAD+x} ]; then GIT_PROMPT_SYMBOLS_AHEAD="↑·"; fi             # The symbol for "n versions ahead of origin"
  if [ -z ${GIT_PROMPT_SYMBOLS_BEHIND+x} ]; then GIT_PROMPT_SYMBOLS_BEHIND="↓·"; fi            # The symbol for "n versions behind of origin"
  if [ -z ${GIT_PROMPT_SYMBOLS_PREHASH+x} ]; then GIT_PROMPT_SYMBOLS_PREHASH=":"; fi            # Written before hash of commit, if no name could be found
  if [ -z ${GIT_PROMPT_SYMBOLS_NO_REMOTE_TRACKING+x} ]; then GIT_PROMPT_SYMBOLS_NO_REMOTE_TRACKING="L"; fi # This symbol is written after the branch, if the branch is not tracked
}

define_undefined_git_prompt_colors


function updatePrompt() {
  local -a git_status_fields
  git_status_fields=($("$__GIT_STATUS_CMD" 2>/dev/null))

  export GIT_BRANCH=$(replaceSymbols ${git_status_fields[0]})
  local GIT_REMOTE="$(replaceSymbols ${git_status_fields[1]})"
  if [[ "." == "$GIT_REMOTE" ]]; then
    unset GIT_REMOTE
  fi

  local GIT_UPSTREAM_PRIVATE="${git_status_fields[2]}"
  if [[ -z "${__GIT_PROMPT_SHOW_UPSTREAM}" || "^" == "$GIT_UPSTREAM_PRIVATE" ]]; then
    unset GIT_UPSTREAM
  else
    export GIT_UPSTREAM=${GIT_UPSTREAM_PRIVATE}
    local GIT_FORMATTED_UPSTREAM="${GIT_PROMPT_UPSTREAM//_UPSTREAM_/\$GIT_UPSTREAM}"
  fi

  local GIT_STAGED=${git_status_fields[3]}
  local GIT_CONFLICTS=${git_status_fields[4]}
  local GIT_CHANGED=${git_status_fields[5]}
  local GIT_UNTRACKED=${git_status_fields[6]}
  local GIT_STASHED=${git_status_fields[7]}
  local GIT_CLEAN=${git_status_fields[8]}

  if [[ -n "$git_status_fields" ]]; then
    local STATUS_PREFIX="${GIT_PROMPT_BRANCH}${GIT_BRANCH}${GIT_FORMATTED_UPSTREAM}"
    local STATUS=""

    # __add_status KIND VALEXPR INSERT
    # eg: __add_status  'STAGED' '-ne 0'

    __chk_gitvar_status() {
      local v
      if [[ "x$2" == "x-n" ]] ; then
        v="$2 \"\$GIT_$1\""
      else
        v="\$GIT_$1 $2"
      fi
      if eval "test $v" ; then
        if [[ $# -lt 2 || "$3" != '-' ]] && [[ "x$__GIT_PROMPT_SHOW_CHANGED_FILES_COUNT" == "x1" || "x$1" == "xREMOTE" ]]; then
          __add_status "\$GIT_PROMPT_$1\$GIT_$1"
        else
          __add_status "\$GIT_PROMPT_$1"
        fi
      fi
    }

    __add_gitvar_status() {
      __add_status "\$GIT_PROMPT_$1\$GIT_$1"
    }

    # __add_status SOMETEXT
    __add_status() {
      eval "STATUS=\"$STATUS$1\""
    }

    __chk_gitvar_status 'REMOTE'     '-n'
    __add_status        "$GIT_PROMPT_SEPARATOR"
    __chk_gitvar_status 'STAGED'     '-ne 0'
    __chk_gitvar_status 'CONFLICTS'  '-ne 0'
    __chk_gitvar_status 'CHANGED'    '-ne 0'
    __chk_gitvar_status 'UNTRACKED'  '-ne 0'
    __chk_gitvar_status 'STASHED'    '-ne 0'
    __chk_gitvar_status 'CLEAN'      '-eq 1'   -

    NEW_PROMPT="$STATUS_PREFIX$STATUS"
  else
    NEW_PROMPT="$EMPTY_PROMPT"
  fi

  echo $NEW_PROMPT
}
GIT_INFO=$(updatePrompt)

printf "\033]"
printf "1337;SetKeyLabel=%s=%s" "status" "$GIT_INFO"
printf "\a"
